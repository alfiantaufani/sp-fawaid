<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('dosen_masuk') != TRUE) {
            redirect('auth');
        }
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('dosen', ['id' => $this->session->userdata('dosen_id')])->row_array();

        $this->db->select('*');
        $this->db->from('jadwal');
        $this->db->join('mata_kuliah', 'jadwal.id_matkul=mata_kuliah.id');
        $this->db->join('dosen', 'jadwal.id_dosen=dosen.id');
        $this->db->join('kelas', 'jadwal.id_kelas=kelas.id');
        $this->db->join('semester', 'jadwal.id_semester=semester.id');
        $jadwal = $this->db->get();

        $data['jadwal'] = $jadwal->result();
        $data['matkul'] = $this->db->get('mata_kuliah')->result();
        $data['dosen'] = $this->db->get_where('dosen', array('id !=' => '3'))->result();
        $data['kelas'] = $this->db->get('kelas')->result();
        $data['semester'] = $this->db->get('semester')->result();
        $data['hari'] = $this->hari();
        $data['jam_ke'] = $this->jam_ke();
        // echo json_encode($data['jadwal']);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/jadwal', $data);
        $this->load->view('templates/footer', $data);
    }

    public function store()
    {
        $cek = $this->db->get_where(
            'jadwal',
            array(
                'id_matkul' => $this->input->post('id_matkul'),
                'id_dosen' => $this->input->post('id_dosen'),
                'id_kelas' => $this->input->post('id_kelas'),
                'id_semester' => $this->input->post('id_semester'),
                'hari' => $this->input->post('hari'),
                'jam_ke' => $this->input->post('jam_ke'),
            )
        );

        if ($cek->num_rows() > 0) {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Jadwal sudah ada!</div>');
            redirect('dosen/jadwal');
        }

        $data = [
            'id_matkul' => $this->input->post('id_matkul'),
            'id_dosen' => $this->input->post('id_dosen'),
            'id_kelas' => $this->input->post('id_kelas'),
            'id_semester' => $this->input->post('id_semester'),
            'id_ketentuan' => '1',
            'hari' => $this->input->post('hari'),
            'jam_ke' => $this->input->post('jam_ke'),
        ];

        $insert = $this->db->insert('jadwal', $data);
        if ($insert) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil tambah Jadwal</div>');
            redirect('dosen/jadwal');
        } else {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal tambah Jadwal</div>');
            redirect('dosen/jadwal');
        }
    }

    public function destroy($id)
    {
        $delete = $this->db->delete('jadwal', array('id_jadwal' => $id));
        if ($delete) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil hapus Jadwal</div>');
            redirect('dosen/jadwal');
        } else {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal hapus Jadwal</div>');
            redirect('dosen/jadwal');
        }
    }

    public function update()
    {
        $data = [
            'id_matkul' => $this->input->post('id_matkul'),
            'id_dosen' => $this->input->post('id_dosen'),
            'id_kelas' => $this->input->post('id_kelas'),
            'id_semester' => $this->input->post('id_semester'),
            'hari' => $this->input->post('hari'),
            'jam_ke' => $this->input->post('jam_ke'),
        ];

        $this->db->where('id_jadwal', $this->input->post('id'));
        $update = $this->db->update('jadwal', $data);
        if ($update) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil ubah Jadwal</div>');
            redirect('dosen/jadwal');
        } else {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal ubah Jadwal</div>');
            redirect('dosen/jadwal');
        }
    }

    public function hari()
    {
        $hari = [
            [
                'hari' => 'Senin',
            ],
            [
                'hari' => 'Selasa',
            ],
            [
                'hari' => 'Rabu',
            ],
            [
                'hari' => 'Kamis',
            ],
            [
                'hari' => "Jum'at",
            ],
            [
                'hari' => "Sabtu",
            ],
            [
                'hari' => "Ahad",
            ],
        ];
        return $hari;
    }

    public function jam_ke()
    {
        $hari = [
            [
                'jam_ke' => 'I', 'jam' => 'jam 07.00 - 08.00'
            ],
            [
                'jam_ke' => 'II', 'jam' => 'jam 08.00 - 09.00'
            ],
            [
                'jam_ke' => 'III', 'jam' => 'jam 09.00 - 10.00'
            ],
            [
                'jam_ke' => 'IV', 'jam' => 'jam 11.00 - 12.00'
            ],
            [
                'jam_ke' => "V", 'jam' => 'jam 12.00 - 13.00'
            ],
            [
                'jam_ke' => "VI", 'jam' => 'jam 13.00 - 14.00'
            ]
        ];
        return $hari;
    }
}
