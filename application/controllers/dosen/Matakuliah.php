<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Matakuliah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('dosen_masuk') != TRUE){
			redirect('auth');
		}
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('dosen', ['id' => $this->session->userdata('dosen_id')])->row_array();

        $data['matakuliah'] = $this->db->get('mata_kuliah')->result();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/matakuliah', $data);
        $this->load->view('templates/footer', $data);
    }

    public function store()
    {
        $data = [
            'kode_matkul' => $this->input->post('kode_matkul'),
            'nama_matkul' => $this->input->post('nama_matkul'),
            'sks_matkul' => $this->input->post('sks_matkul'),
        ];

        $insert = $this->db->insert('mata_kuliah', $data);
        if ($insert) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil tambah mata kuliah</div>');
            redirect('dosen/mata-kuliah');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal tambah mata kuliah</div>');
            redirect('dosen/mata-kuliah');
        }
    }

    public function destroy($id)
    {
        $cek = $this->db->get_where('jadwal', ['id_matkul' => $id])->num_rows();
        if ($cek > 0) {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal! Data Matakuliah masih digunakan di jadwal!</div>');
            redirect('dosen/mata-kuliah');
        }

        $delete = $this->db->delete('mata_kuliah', array('id' => $id));
        if ($delete) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil hapus mata kuliah</div>');
            redirect('dosen/mata-kuliah');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal hapus mata kuliah</div>');
            redirect('dosen/mata-kuliah');
        }
    }

    public function update()
    {
        $data = [
            'kode_matkul' => $this->input->post('kode_matkul'),
            'nama_matkul' => $this->input->post('nama_matkul'),
            'sks_matkul' => $this->input->post('sks_matkul'),
        ];

        $this->db->where('id', $this->input->post('id'));
        $update = $this->db->update('mata_kuliah', $data);
        if ($update) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil ubah mata kuliah</div>');
            redirect('dosen/mata-kuliah');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal ubah mata kuliah</div>');
            redirect('dosen/mata-kuliah');
        }
    }
}
