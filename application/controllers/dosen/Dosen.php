<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('dosen_masuk') != TRUE){
			redirect('auth');
		}
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('dosen', ['id' => $this->session->userdata('dosen_id')])->row_array();

        $data['dosen'] = $this->db->get_where('dosen', ['id !=' => $this->session->userdata('dosen_id')])->result();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/dosen', $data);
        $this->load->view('templates/footer', $data);
    }

    public function store()
    {
        $cek_username = $this->db->get_where('dosen', array('username' => $this->input->post('username')));
        if ($cek_username->num_rows() > 0) {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Username sudah digunakan</div>');
            redirect('dosen/dosen');
        }

        $data = [
            'nama' => $this->input->post('nama'),
            'nidn' => $this->input->post('nidn'),
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
        ];

        $insert = $this->db->insert('dosen', $data);
        if ($insert) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil tambah data dosen</div>');
            redirect('dosen/dosen');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal tambah data dosen</div>');
            redirect('dosen/dosen');
        }
    }

    public function destroy($id)
    {
        $cek = $this->db->get_where('jadwal', ['id_dosen' => $id])->num_rows();
        if ($cek > 0) {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal! Data dosen masih digunakan di jadwal!</div>');
            redirect('dosen/dosen');
        }
        $delete = $this->db->delete('dosen', array('id' => $id));
        if ($delete) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil hapus data dosen</div>');
            redirect('dosen/dosen');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal hapus data dosen</div>');
            redirect('dosen/dosen');
        }
    }

    public function update()
    {
        if ($this->input->post('password') == "") {
            $data = [
                'nama' => $this->input->post('nama'),
                'nidn' => $this->input->post('nidn'),
                'email' => $this->input->post('email'),
                'username' => $this->input->post('username'),
            ];
        }else{
            $data = [
                'nama' => $this->input->post('nama'),
                'nidn' => $this->input->post('nidn'),
                'email' => $this->input->post('email'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
            ];
        }

        $this->db->where('id', $this->input->post('id'));
        $update = $this->db->update('dosen', $data);
        if ($update) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil ubah data dosen</div>');
            redirect('dosen/dosen');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal ubah data dosen</div>');
            redirect('dosen/dosen');
        }
    }
}
