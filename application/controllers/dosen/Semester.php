<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Semester extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('dosen_masuk') != TRUE){
			redirect('auth');
		}
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('dosen', ['id' => $this->session->userdata('dosen_id')])->row_array();

        $data['semester'] = $this->db->get('semester')->result();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/semester', $data);
        $this->load->view('templates/footer', $data);
    }

    public function store()
    {
        $data = [
            'semester' => $this->input->post('semester'),
        ];

        $insert = $this->db->insert('semester', $data);
        if ($insert) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil tambah data semester</div>');
            redirect('dosen/semester');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal tambah data semester</div>');
            redirect('dosen/semester');
        }
    }

    public function destroy($id)
    {
        $cek = $this->db->get_where('jadwal', ['id_semester' => $id])->num_rows();
        if ($cek > 0) {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal! Data Semester masih digunakan di jadwal!</div>');
            redirect('dosen/semester');
        }

        $delete = $this->db->delete('semester', array('id' => $id));
        if ($delete) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil hapus data semester</div>');
            redirect('dosen/semester');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal hapus data semester</div>');
            redirect('dosen/semester');
        }
    }

    public function update()
    {
        $data = [
            'semester' => $this->input->post('semester'),
        ];

        $this->db->where('id', $this->input->post('id'));
        $update = $this->db->update('semester', $data);
        if ($update) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil ubah data semester</div>');
            redirect('dosen/semester');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal ubah data semester</div>');
            redirect('dosen/semester');
        }
    }
}
