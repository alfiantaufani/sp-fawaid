<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('mahasiswa_masuk') != TRUE){
			redirect('auth');
		}
        $this->load->library('user_agent');
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->library('upload','session', 'database');
    }

    public function index()
    {
        $data['title'] = 'Beranda';
        $data['user'] = $this->db->get_where('mahasiswa', ['id' => $this->session->userdata('mahasiswa_id')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('mahasiswa/index', $data);
        $this->load->view('templates/footer', $data);
		
    }

    public function jadwal()
    {
        $data['title'] = 'Beranda';

        $this->db->select('*');
        $this->db->from('mahasiswa');
        $this->db->join('kelas', 'mahasiswa.id_kelas=kelas.id');
        $this->db->where('mahasiswa.id', $this->session->userdata('mahasiswa_id'));
        $user = $this->db->get();

        $data['user'] = $user->row_array();
        $mahasiswa = $data['user'];
        
        $this->db->select('*');
        $this->db->from('jadwal');
        $this->db->join('mata_kuliah', 'jadwal.id_matkul=mata_kuliah.id');
        $this->db->join('dosen', 'jadwal.id_dosen=dosen.id');
        $this->db->join('kelas', 'jadwal.id_kelas=kelas.id');
        $this->db->where('jadwal.id_kelas', $mahasiswa['id_kelas']);
        $jadwal = $this->db->get();

        $data['jadwal'] = $jadwal->result();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('mahasiswa/jadwal', $data);
        $this->load->view('templates/footer', $data);
    }
}
