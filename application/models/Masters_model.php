<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masters_model extends CI_Model
{
    public function data_pembayaran()
    {
        //return $this->db->get('pembayaran')->result_array();
        $this->db->select(' pembayaran.id AS id_pembayaran, 
                            pembayaran.bulan_bayar AS bulan_bayar, 
                            pembayaran.besaran AS besaran, 
                            pembayaran.tahun AS tahun, 
                            pembayaran.paket_kelas_id AS paket_kelas_id',  FALSE);
        $this->db->from('pembayaran'); 
        $this->db->join('paket_kelas', 'pembayaran.paket_kelas_id = paket_kelas.id');
        $this->db->order_by("pembayaran.id","desc");
        
        $result= $this->db->get()->result_array();
		return $result;  
    }
    public function data_tagihan()
    {
        $this->db->select('*');
        $this->db->from('tagihan');
        $this->db->join('pembayaran', 'tagihan.id_pembayaran = pembayaran.id');
        $this->db->join('paket_kelas', 'pembayaran.paket_kelas_id = paket_kelas.id');
        $this->db->order_by("tagihan.id_tagihan","desc");
        $result= $this->db->get()->result_array();
		return $result;  
    }

    public function tampil_siswa_sesuai_paket($paket_kelas)
    {
        $this->db->select('*');
        $this->db->from('data_siswa');
        $this->db->join('paket_kelas', 'data_siswa.paket_kelas_id = paket_kelas.id');
        $this->db->where('data_siswa.paket_kelas_id', $paket_kelas);
        $result= $this->db->get()->result_array();
		return $result;
    }

    public function data_paket_kelas()
    {
        $this->db->select('*');
        $this->db->from('paket_kelas');
        $result= $this->db->get()->result_array();
		return $result;
    }

    public function data_siswa()
    {
        $this->db->select('*');
        $this->db->from('data_siswa');
        $this->db->join('paket_kelas', 'data_siswa.paket_kelas_id = paket_kelas.id');
        $this->db->order_by("data_siswa.id_siswa","desc");
        $result= $this->db->get()->result_array();
		return $result;  
    }

    public function data_siswa_edit($id_siswa)
    {
        $this->db->select('*');
        $this->db->from('data_siswa');
        $this->db->join('paket_kelas', 'data_siswa.paket_kelas_id = paket_kelas.id');
        $this->db->where('data_siswa.id_siswa',$id_siswa);
        $result= $this->db->get()->row_array();
		return $result;  
    }

    public function tambahKelas()
    {
        $data = [
            'nama_kelas' => $this->input->post('nama_kelas', true),
            'id_kurikulum' => $this->input->post('id_kurikulum', true)
        ];

        $this->db->insert('kelas', $data);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                data kelas berhasil ditambah!</div>
                ');
        redirect('admin/master');
    }

    public function hapusKelas($id)
    {
        $result = $this->db->get_where('kelas', ['id' => $id]);
        if (!$result) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                data kelas gagal dihapus! data tidak ditemukan</div>
                ');
            redirect('admin/master');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('kelas');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                data kelas berhasil dihapus!</div>
                ');
            redirect('admin/master');
        }
    }

    public function getDataIuranById($id)
    {
        return $this->db->get_where('iuran', ['id' => $id])->row_array();
    }

    public function getDataKurikulumById($id)
    {
        return $this->db->get_where('kurikulum', ['id' => $id])->row_array();
    }

    public function getDataKelasById($id)
    {
        return $this->db->get_where('kelas', ['id' => $id])->row_array();
    }

    public function edit_siswa($id_siswa)
    {
        
    }

    public function editKelas($id)
    {
        $cekKelas = $this->db->get_where('kelas', ['id' => $id]);
        if (!$cekKelas) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    data kelas gagal diedit! data tidak ditemukan</div>
                    ');
            redirect('admin/master');
        } else {
            $data = [
                'nama_kelas' => $this->input->post('nama_kelas', true),
                'id_kurikulum' => $this->input->post('id_kurikulum', true)
            ];

            $this->db->set($data);
            $this->db->where('id', $id);
            $this->db->update('kelas');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    data kelas berhasil diedit!</div>
                    ');
            redirect('admin/master');
        }
    }
}
