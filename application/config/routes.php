<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Dosen
$route['dosen/dashboard'] = 'dosen/Admin';

$route['dosen/mata-kuliah'] = 'dosen/Matakuliah';
$route['dosen/mata-kuliah/store'] = 'dosen/Matakuliah/store';
$route['dosen/mata-kuliah/update'] = 'dosen/Matakuliah/update';
$route['dosen/mata-kuliah/destroy/(:any)'] = 'dosen/Matakuliah/destroy/$1';

$route['dosen/jadwal'] = 'dosen/Jadwal';
$route['dosen/jadwal/store'] = 'dosen/Jadwal/store';
$route['dosen/jadwal/update'] = 'dosen/Jadwal/update';
$route['dosen/jadwal/destroy/(:any)'] = 'dosen/Jadwal/destroy/$1';

$route['dosen/kelas'] = 'dosen/Kelas';
$route['dosen/kelas/store'] = 'dosen/Kelas/store';
$route['dosen/kelas/update'] = 'dosen/Kelas/update';
$route['dosen/kelas/destroy/(:any)'] = 'dosen/Kelas/destroy/$1';

$route['dosen/semester'] = 'dosen/Semester';
$route['dosen/semester/store'] = 'dosen/Semester/store';
$route['dosen/semester/update'] = 'dosen/Semester/update';
$route['dosen/semester/destroy/(:any)'] = 'dosen/Semester/destroy/$1';

$route['dosen/mahasiswa'] = 'dosen/Mahasiswa';
$route['dosen/mahasiswa/store'] = 'dosen/Mahasiswa/store';
$route['dosen/mahasiswa/update'] = 'dosen/Mahasiswa/update';
$route['dosen/mahasiswa/destroy/(:any)'] = 'dosen/Mahasiswa/destroy/$1';

$route['dosen/dosen'] = 'dosen/Dosen';
$route['dosen/dosen/store'] = 'dosen/Dosen/store';
$route['dosen/dosen/update'] = 'dosen/Dosen/update';
$route['dosen/dosen/destroy/(:any)'] = 'dosen/Dosen/destroy/$1';

// Mahaiswa
$route['mahasiswa'] = 'Mahasiswa';
$route['mahasiswa/jadwal'] = 'Mahasiswa/jadwal';