<div class="content-wrapper">
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

        <!-- menampilkan pesan -->
        <div class="row">
            <div class="col-12">
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('success'); ?>
                <?= $this->session->flashdata('errors'); ?>
            </div>
        </div>

        <!-- card data pembayaran -->
        <div class="row">
            <div class="col">
                <div class="card shadow-lg mb-3">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Matakuliah</h3> 
                        <div class="card-tools">
                            <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#tambah">Tambah Data Matakuliah</button>
                        </div> 
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="tableIuran">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Kode Matkul</th>
                                        <th scope="col">Matakuliah</th>
                                        <th scope="col">SKS</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($matakuliah as $value) : ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $value->kode_matkul ?></td>
                                            <td><?= $value->nama_matkul ?></td>
                                            <td><?= $value->sks_matkul ?></td>
                                            <td>
                                                <a href="javascript:void(0)" class="btn btn-info mr-1" onclick="edit(this)" data-id="<?= $value->id ?>" data-kodematkul="<?= $value->kode_matkul ?>" data-namamatkul="<?= $value->nama_matkul ?>" data-sksmatkul="<?= $value->sks_matkul ?>"><i class="fas fa-edit fa-sm"></i> Edit</a>

                                                <a href="<?=base_url('dosen/mata-kuliah/destroy')?>/<?= $value->id; ?>" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?');"><i class="fas fa-trash-alt fa-sm"></i> Hapus</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card data pembayaran -->
                                    
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Data Matakuliah</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <form method="post" action="<?= base_url('dosen/mata-kuliah/store'); ?>">
                    <div class="row">
                        <div class="col-lg">
                            <div class="form-group">
                                <label for="">Kode matkul</label>
                                <input type="text" name="kode_matkul" class="form-control" placeholder="Masukkan kode matkul" require>
                            </div>
                            <div class="form-group">
                                <label for="">Nama matkul</label>
                                <input type="text" name="nama_matkul" class="form-control" placeholder="Masukkan nama matkul" require>
                            </div>
                            <div class="form-group">
                                <label for="">SKS matkul</label>
                                <input type="number" name="sks_matkul" class="form-control" placeholder="Masukkan sks matkul" require>
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- akhir form input -->

            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Paket Kelas</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" action="<?= base_url('dosen/mata-kuliah/update'); ?>">
                    <div class="row">
                        <div class="col-lg">
                            <input type="hidden" name="id" id="id">
                            <div class="form-group">
                                <label for="">Kode matkul</label>
                                <input type="text" name="kode_matkul" id="kode_matkul" class="form-control" placeholder="Masukkan kode matkul" require>
                            </div>
                            <div class="form-group">
                                <label for="">Nama matkul</label>
                                <input type="text" name="nama_matkul" id="nama_matkul" class="form-control" placeholder="Masukkan nama matkul" require>
                            </div>
                            <div class="form-group">
                                <label for="">SKS matkul</label>
                                <input type="number" name="sks_matkul" id="sks_matkul" class="form-control" placeholder="Masukkan sks matkul" require>
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<script>
    function edit(el) {
        $('#edit').modal('show');
        $('#id').val($(el).data('id'));
        $('#kode_matkul').val($(el).data('kodematkul'));
        $('#nama_matkul').val($(el).data('namamatkul'));
        $('#sks_matkul').val($(el).data('sksmatkul'));
    }
</script>