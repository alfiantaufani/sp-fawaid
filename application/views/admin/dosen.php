
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Dosen</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active"><a href="dosen.php">Data Dosen</a></li>
                  </ol>
              </div>
          </div>
      </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('success'); ?>
                <?= $this->session->flashdata('errors'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Dosen</h3> 
                        <div class="card-tools">
                            <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#tambah">Tambah Data Dosen</button>
                        </div> 
                    </div>  
                    
                    <div class="card-body table-responsive p-0" style="height: 300px;">
                        <table class="table table-head-fixed text-nowrap">
                            <thead>
                                <tr>
                                  <th>NO</th>
                                  <th>ID DOSEN</th>
                                  <th>NAMA DOSEN</th>
                                  <th>EMAIL</th>
                                  <th>USERNAME</th>
                                  <th>AKSI</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($dosen as $value) : ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $value->nidn ?></td>
                                    <td><?= $value->nama ?></td>
                                    <td><?= $value->email ?></td>
                                    <td><?= $value->username ?></td>
                                    <td>
                                        <a href="javascript:void(0)" onclick="edit(this)" data-id="<?= $value->id ?>" data-nama="<?= $value->nama ?>" data-nidn="<?= $value->nidn ?>" data-email="<?= $value->email ?>" data-username="<?= $value->username ?>"> Edit</a>
                                        |
                                        <a href="<?=base_url('dosen/dosen/destroy')?>/<?= $value->id; ?>" onclick="return confirm('Yakin ingin menghapus data ini?');"> Hapus</a>
                                    </td>
                                </tr>
                                <?php endforeach;    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>    
</div>

<div class="modal fade" id="tambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Dosen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="<?= base_url('dosen/dosen/store'); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nama Dosen</label>
                        <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Dosen" require>
                    </div>
                    <div class="form-group">
                        <label for="">NIDN</label>
                        <input type="number" name="nidn" class="form-control" placeholder="Masukkan NIDN" require>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Masukkan email" require>
                    </div>
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Masukkan username" require>
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Masukkan password" require>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-success" name="tambah" value="simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data Dosen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="<?= base_url('dosen/dosen/update'); ?>">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nama Dosen</label>
                        <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama Dosen" require>
                    </div>
                    <div class="form-group">
                        <label for="">NIDN</label>
                        <input type="number" name="nidn" id="nidn" class="form-control" placeholder="Masukkan NIDN" require>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Masukkan email" require>
                    </div>
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" name="username" id="username"  class="form-control" placeholder="Masukkan username" require>
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Masukkan password" require>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-success" name="tambah" value="simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function edit(el) {
        $('#edit').modal('show');
        $('#id').val($(el).data('id'));
        $('#nama').val($(el).data('nama'));
        $('#nidn').val($(el).data('nidn'));
        $('#email').val($(el).data('email'));
        $('#username').val($(el).data('username'));
    }
</script>
