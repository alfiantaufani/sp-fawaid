<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-primary"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>

    <!-- row untuk jadi satu baris card -->
    <div class="row">
        <div class="col-md-4">
            <div class="card shadow-lg mb-2">
                <div class="row no-gutters">
                    <div class="col-md-3">
                        <img src="
                            <?php 
                                if ($user['img_siswa'] == NULL) {
                                    echo base_url('assets/img/profile/profile.jpg');
                                }else{
                                    echo base_url('assets/img/profile/') . $user['img_siswa'];
                                }
                            ?>
                        " class="card-img" alt="profile">
                    </div>
                    <div class="col-md-9">
                        <div class="card-body">
                            <!-- <h5 class="card-title"><strong>Profil Siswa</strong></h5> -->
                            <p class="card-text"><b>Nama :</b>  <?= $user['nama_siswa']; ?></p>
                            <p class="card-text"><b>Email :</b>  <?= $user['email']; ?></p>
                            <p class="card-text"><b>Terdaftar pada tanggal :</b>  <?php echo format_indo($user['created_at']);?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Data Siswa</h4>
                    <a class="btn btn-primary shadow-lg" href="#"><i class="fas fa-lock fa-sm"></i> Edit Password</a>
                </div>
                <div class="card-body">
                    <?php echo form_open_multipart('user/edit_siswa');?>
                        <div class="row">
                            
                            <div class="col-lg">
                                <div class="form-group">
                                    <label for="nik">Nomor Induk Kependudakan (NIK)</label>
                                    <input type="text" class="form-control" name="nik" id="nik" value="<?= $user['nik']; ?>" readonly>
                                    <input type="hidden" name="email" value="<?=$user['email'];?>">
                                </div>
                                <div class="form-group">
                                    <label for="nama_siswa">Nama Siswa</label>
                                    <input type="text" class="form-control" name="nama_siswa" id="nama_siswa" value="<?= $user['nama_siswa']; ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="nama_siswa">Jenis Kelamin</label>
                                    <select name="jenis_kelamin" id="jk" class="form-control" disabled>
                                        <option value="<?= $user['jenis_kelamin']; ?>"  selected hidden><?= $user['jenis_kelamin']; ?></option>
                                        <option value="Perempuan">Perempuan</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label for="kelas">Paket Kelas</label>
                                    <select name="paket_kelas" id="paket_kelas" class="form-control" disabled>
                                        <option value="<?= $user['id'] ?>" selected hidden><?= $user['nama'] ?></option>
                                        <?php foreach ($kelas as $data) :?>
                                            <option value="<?= $data['id'] ?>"><?= $data['nama'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nama_ayah">Nama Ayah</label>
                                    <input type="text" class="form-control" name="nama_ayah" value="<?= $user['nama_ayah']; ?>" id="nama_ayah" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="nama_ibu">Nama Ibu</label>
                                    <input type="text" class="form-control" name="nama_ibu" id="nama_ibu" value="<?= $user['nama_ibu']; ?>" readonly>
                                </div>
                                
                                
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="alamat_ortu">Alamat Lengkap Orang Tua</label>
                                    <textarea class="form-control" name="alamat_ortu" id="alamat_ortu" rows="2" readonly><?= $user['alamat_ortu']; ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                            <hr>
                                <div class="form-group float-left">
                                    <button type="button" class="btn btn-primary shadow-lg" id="edt" onclick="edit()">Edit Data Siswa</button>
                                    <button type="submit" class="btn btn-primary shadow-lg" id="simpan" style="display:none;">Simpan</button>

                                    <button type="reset" class="btn btn-outline-primary ml-2" id="batal" role="button" style="display:none;" onclick="btl()">Batal</button>
                                </div>
                            </div>
                        
                        </div>
                    <?php echo form_close();?>
                </div>
            </div> 
        </div>                   
    </div>
    <!-- /.end raw card -->

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    
    function edit() {
        //var edit = $('#edit');
        $('#simpan').show();
        $('#batal').show();
        $('#edt').hide();

        $('#nik').attr('readonly', false);
        $('#nama_siswa').attr('readonly', false);
        
        $('#jk').removeAttr('disabled');
        $('#paket_kelas').removeAttr('disabled');
        $('#nama_ayah').attr('readonly', false);
        $('#nama_ibu').attr('readonly', false);
        $('#alamat_ortu').attr('readonly', false);
    }
    
    function btl() {
        $('#simpan').hide();
        $('#batal').hide();
        $('#edt').show();

        $('#nik').attr('readonly', true);
        $('#nama_siswa').attr('readonly', true);
        
        $('#jk').attr('disabled', 'disabled');
        $('#paket_kelas').attr('disabled', 'disabled');
        $('#nama_ayah').attr('readonly', true);
        $('#nama_ibu').attr('readonly', true);
        $('#alamat_ortu').attr('readonly', true);
    }
    
</script>