<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

    <!-- card data pembayaran -->
    <div class="row">
        <div class="col">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Data Jadwal Kelas : <?= $user['kelas'] ?></h4>
                </div>
                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableIuran">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Kode MK</th>
                                    <th scope="col">Matakuliah</th>
                                    <th scope="col">SKS</th>
                                    <th scope="col">Jadwal</th>
                                    <th scope="col">Kelas</th>
                                    <th scope="col">Dosen</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($jadwal as $value) : ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $value->kode_matkul ?></td>
                                        <td><?= $value->nama_matkul ?></td>
                                        <td><?= $value->sks_matkul ?></td>
                                        <td><?= $value->hari ?>, <?= $value->jam_ke ?> (<?= $value->jam_mulai ?>-<?= $value->jam_selesai ?>)</td>
                                        <td><?= $value->kelas ?></td>
                                        <td><?= $value->nama ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card data pembayaran -->
                                
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
